package br.com.bomdestino.sgm.domain;

import static org.assertj.core.api.Assertions.assertThat;

import br.com.bomdestino.sgm.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ImovelTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Imovel.class);
        Imovel imovel1 = new Imovel();
        imovel1.setId(1L);
        Imovel imovel2 = new Imovel();
        imovel2.setId(imovel1.getId());
        assertThat(imovel1).isEqualTo(imovel2);
        imovel2.setId(2L);
        assertThat(imovel1).isNotEqualTo(imovel2);
        imovel1.setId(null);
        assertThat(imovel1).isNotEqualTo(imovel2);
    }
}
