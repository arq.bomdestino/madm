package br.com.bomdestino.sgm.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.com.bomdestino.sgm.IntegrationTest;
import br.com.bomdestino.sgm.domain.Imovel;
import br.com.bomdestino.sgm.domain.enumeration.TipoImovel;
import br.com.bomdestino.sgm.repository.ImovelRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ImovelResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ImovelResourceIT {

    private static final String DEFAULT_COD_IMOVEL = "AAAAAAAAAA";
    private static final String UPDATED_COD_IMOVEL = "BBBBBBBBBB";

    private static final String DEFAULT_DOCUMENTO_PROPRIETARIO = "AAAAAAAAAA";
    private static final String UPDATED_DOCUMENTO_PROPRIETARIO = "BBBBBBBBBB";

    private static final TipoImovel DEFAULT_TIPO_IMOVEL = TipoImovel.CASA;
    private static final TipoImovel UPDATED_TIPO_IMOVEL = TipoImovel.PREDIO;

    private static final String ENTITY_API_URL = "/api/imovels";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ImovelRepository imovelRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restImovelMockMvc;

    private Imovel imovel;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Imovel createEntity(EntityManager em) {
        Imovel imovel = new Imovel()
            .codImovel(DEFAULT_COD_IMOVEL)
            .documentoProprietario(DEFAULT_DOCUMENTO_PROPRIETARIO)
            .tipoImovel(DEFAULT_TIPO_IMOVEL);
        return imovel;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Imovel createUpdatedEntity(EntityManager em) {
        Imovel imovel = new Imovel()
            .codImovel(UPDATED_COD_IMOVEL)
            .documentoProprietario(UPDATED_DOCUMENTO_PROPRIETARIO)
            .tipoImovel(UPDATED_TIPO_IMOVEL);
        return imovel;
    }

    @BeforeEach
    public void initTest() {
        imovel = createEntity(em);
    }

    @Test
    @Transactional
    void createImovel() throws Exception {
        int databaseSizeBeforeCreate = imovelRepository.findAll().size();
        // Create the Imovel
        restImovelMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(imovel)))
            .andExpect(status().isCreated());

        // Validate the Imovel in the database
        List<Imovel> imovelList = imovelRepository.findAll();
        assertThat(imovelList).hasSize(databaseSizeBeforeCreate + 1);
        Imovel testImovel = imovelList.get(imovelList.size() - 1);
        assertThat(testImovel.getCodImovel()).isEqualTo(DEFAULT_COD_IMOVEL);
        assertThat(testImovel.getDocumentoProprietario()).isEqualTo(DEFAULT_DOCUMENTO_PROPRIETARIO);
        assertThat(testImovel.getTipoImovel()).isEqualTo(DEFAULT_TIPO_IMOVEL);
    }

    @Test
    @Transactional
    void createImovelWithExistingId() throws Exception {
        // Create the Imovel with an existing ID
        imovel.setId(1L);

        int databaseSizeBeforeCreate = imovelRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restImovelMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(imovel)))
            .andExpect(status().isBadRequest());

        // Validate the Imovel in the database
        List<Imovel> imovelList = imovelRepository.findAll();
        assertThat(imovelList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllImovels() throws Exception {
        // Initialize the database
        imovelRepository.saveAndFlush(imovel);

        // Get all the imovelList
        restImovelMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(imovel.getId().intValue())))
            .andExpect(jsonPath("$.[*].codImovel").value(hasItem(DEFAULT_COD_IMOVEL)))
            .andExpect(jsonPath("$.[*].documentoProprietario").value(hasItem(DEFAULT_DOCUMENTO_PROPRIETARIO)))
            .andExpect(jsonPath("$.[*].tipoImovel").value(hasItem(DEFAULT_TIPO_IMOVEL.toString())));
    }

    @Test
    @Transactional
    void getImovel() throws Exception {
        // Initialize the database
        imovelRepository.saveAndFlush(imovel);

        // Get the imovel
        restImovelMockMvc
            .perform(get(ENTITY_API_URL_ID, imovel.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(imovel.getId().intValue()))
            .andExpect(jsonPath("$.codImovel").value(DEFAULT_COD_IMOVEL))
            .andExpect(jsonPath("$.documentoProprietario").value(DEFAULT_DOCUMENTO_PROPRIETARIO))
            .andExpect(jsonPath("$.tipoImovel").value(DEFAULT_TIPO_IMOVEL.toString()));
    }

    @Test
    @Transactional
    void getNonExistingImovel() throws Exception {
        // Get the imovel
        restImovelMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewImovel() throws Exception {
        // Initialize the database
        imovelRepository.saveAndFlush(imovel);

        int databaseSizeBeforeUpdate = imovelRepository.findAll().size();

        // Update the imovel
        Imovel updatedImovel = imovelRepository.findById(imovel.getId()).get();
        // Disconnect from session so that the updates on updatedImovel are not directly saved in db
        em.detach(updatedImovel);
        updatedImovel.codImovel(UPDATED_COD_IMOVEL).documentoProprietario(UPDATED_DOCUMENTO_PROPRIETARIO).tipoImovel(UPDATED_TIPO_IMOVEL);

        restImovelMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedImovel.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedImovel))
            )
            .andExpect(status().isOk());

        // Validate the Imovel in the database
        List<Imovel> imovelList = imovelRepository.findAll();
        assertThat(imovelList).hasSize(databaseSizeBeforeUpdate);
        Imovel testImovel = imovelList.get(imovelList.size() - 1);
        assertThat(testImovel.getCodImovel()).isEqualTo(UPDATED_COD_IMOVEL);
        assertThat(testImovel.getDocumentoProprietario()).isEqualTo(UPDATED_DOCUMENTO_PROPRIETARIO);
        assertThat(testImovel.getTipoImovel()).isEqualTo(UPDATED_TIPO_IMOVEL);
    }

    @Test
    @Transactional
    void putNonExistingImovel() throws Exception {
        int databaseSizeBeforeUpdate = imovelRepository.findAll().size();
        imovel.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restImovelMockMvc
            .perform(
                put(ENTITY_API_URL_ID, imovel.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(imovel))
            )
            .andExpect(status().isBadRequest());

        // Validate the Imovel in the database
        List<Imovel> imovelList = imovelRepository.findAll();
        assertThat(imovelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchImovel() throws Exception {
        int databaseSizeBeforeUpdate = imovelRepository.findAll().size();
        imovel.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restImovelMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(imovel))
            )
            .andExpect(status().isBadRequest());

        // Validate the Imovel in the database
        List<Imovel> imovelList = imovelRepository.findAll();
        assertThat(imovelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamImovel() throws Exception {
        int databaseSizeBeforeUpdate = imovelRepository.findAll().size();
        imovel.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restImovelMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(imovel)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Imovel in the database
        List<Imovel> imovelList = imovelRepository.findAll();
        assertThat(imovelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateImovelWithPatch() throws Exception {
        // Initialize the database
        imovelRepository.saveAndFlush(imovel);

        int databaseSizeBeforeUpdate = imovelRepository.findAll().size();

        // Update the imovel using partial update
        Imovel partialUpdatedImovel = new Imovel();
        partialUpdatedImovel.setId(imovel.getId());

        partialUpdatedImovel.tipoImovel(UPDATED_TIPO_IMOVEL);

        restImovelMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedImovel.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedImovel))
            )
            .andExpect(status().isOk());

        // Validate the Imovel in the database
        List<Imovel> imovelList = imovelRepository.findAll();
        assertThat(imovelList).hasSize(databaseSizeBeforeUpdate);
        Imovel testImovel = imovelList.get(imovelList.size() - 1);
        assertThat(testImovel.getCodImovel()).isEqualTo(DEFAULT_COD_IMOVEL);
        assertThat(testImovel.getDocumentoProprietario()).isEqualTo(DEFAULT_DOCUMENTO_PROPRIETARIO);
        assertThat(testImovel.getTipoImovel()).isEqualTo(UPDATED_TIPO_IMOVEL);
    }

    @Test
    @Transactional
    void fullUpdateImovelWithPatch() throws Exception {
        // Initialize the database
        imovelRepository.saveAndFlush(imovel);

        int databaseSizeBeforeUpdate = imovelRepository.findAll().size();

        // Update the imovel using partial update
        Imovel partialUpdatedImovel = new Imovel();
        partialUpdatedImovel.setId(imovel.getId());

        partialUpdatedImovel
            .codImovel(UPDATED_COD_IMOVEL)
            .documentoProprietario(UPDATED_DOCUMENTO_PROPRIETARIO)
            .tipoImovel(UPDATED_TIPO_IMOVEL);

        restImovelMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedImovel.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedImovel))
            )
            .andExpect(status().isOk());

        // Validate the Imovel in the database
        List<Imovel> imovelList = imovelRepository.findAll();
        assertThat(imovelList).hasSize(databaseSizeBeforeUpdate);
        Imovel testImovel = imovelList.get(imovelList.size() - 1);
        assertThat(testImovel.getCodImovel()).isEqualTo(UPDATED_COD_IMOVEL);
        assertThat(testImovel.getDocumentoProprietario()).isEqualTo(UPDATED_DOCUMENTO_PROPRIETARIO);
        assertThat(testImovel.getTipoImovel()).isEqualTo(UPDATED_TIPO_IMOVEL);
    }

    @Test
    @Transactional
    void patchNonExistingImovel() throws Exception {
        int databaseSizeBeforeUpdate = imovelRepository.findAll().size();
        imovel.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restImovelMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, imovel.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(imovel))
            )
            .andExpect(status().isBadRequest());

        // Validate the Imovel in the database
        List<Imovel> imovelList = imovelRepository.findAll();
        assertThat(imovelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchImovel() throws Exception {
        int databaseSizeBeforeUpdate = imovelRepository.findAll().size();
        imovel.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restImovelMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(imovel))
            )
            .andExpect(status().isBadRequest());

        // Validate the Imovel in the database
        List<Imovel> imovelList = imovelRepository.findAll();
        assertThat(imovelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamImovel() throws Exception {
        int databaseSizeBeforeUpdate = imovelRepository.findAll().size();
        imovel.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restImovelMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(imovel)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Imovel in the database
        List<Imovel> imovelList = imovelRepository.findAll();
        assertThat(imovelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteImovel() throws Exception {
        // Initialize the database
        imovelRepository.saveAndFlush(imovel);

        int databaseSizeBeforeDelete = imovelRepository.findAll().size();

        // Delete the imovel
        restImovelMockMvc
            .perform(delete(ENTITY_API_URL_ID, imovel.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Imovel> imovelList = imovelRepository.findAll();
        assertThat(imovelList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
