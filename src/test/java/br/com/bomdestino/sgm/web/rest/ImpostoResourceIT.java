package br.com.bomdestino.sgm.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.com.bomdestino.sgm.IntegrationTest;
import br.com.bomdestino.sgm.domain.Imposto;
import br.com.bomdestino.sgm.domain.enumeration.TipoImposto;
import br.com.bomdestino.sgm.repository.ImpostoRepository;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ImpostoResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ImpostoResourceIT {

    private static final TipoImposto DEFAULT_TIPO_IMPOSTO = TipoImposto.IPTU;
    private static final TipoImposto UPDATED_TIPO_IMPOSTO = TipoImposto.ITBI;

    private static final Double DEFAULT_VALOR_DEVIDO = 1D;
    private static final Double UPDATED_VALOR_DEVIDO = 2D;

    private static final LocalDate DEFAULT_DATA_VENCIMENTO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_VENCIMENTO = LocalDate.now(ZoneId.systemDefault());

    private static final String ENTITY_API_URL = "/api/impostos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ImpostoRepository impostoRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restImpostoMockMvc;

    private Imposto imposto;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Imposto createEntity(EntityManager em) {
        Imposto imposto = new Imposto()
            .tipoImposto(DEFAULT_TIPO_IMPOSTO)
            .valorDevido(DEFAULT_VALOR_DEVIDO)
            .dataVencimento(DEFAULT_DATA_VENCIMENTO);
        return imposto;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Imposto createUpdatedEntity(EntityManager em) {
        Imposto imposto = new Imposto()
            .tipoImposto(UPDATED_TIPO_IMPOSTO)
            .valorDevido(UPDATED_VALOR_DEVIDO)
            .dataVencimento(UPDATED_DATA_VENCIMENTO);
        return imposto;
    }

    @BeforeEach
    public void initTest() {
        imposto = createEntity(em);
    }

    @Test
    @Transactional
    void createImposto() throws Exception {
        int databaseSizeBeforeCreate = impostoRepository.findAll().size();
        // Create the Imposto
        restImpostoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(imposto)))
            .andExpect(status().isCreated());

        // Validate the Imposto in the database
        List<Imposto> impostoList = impostoRepository.findAll();
        assertThat(impostoList).hasSize(databaseSizeBeforeCreate + 1);
        Imposto testImposto = impostoList.get(impostoList.size() - 1);
        assertThat(testImposto.getTipoImposto()).isEqualTo(DEFAULT_TIPO_IMPOSTO);
        assertThat(testImposto.getValorDevido()).isEqualTo(DEFAULT_VALOR_DEVIDO);
        assertThat(testImposto.getDataVencimento()).isEqualTo(DEFAULT_DATA_VENCIMENTO);
    }

    @Test
    @Transactional
    void createImpostoWithExistingId() throws Exception {
        // Create the Imposto with an existing ID
        imposto.setId(1L);

        int databaseSizeBeforeCreate = impostoRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restImpostoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(imposto)))
            .andExpect(status().isBadRequest());

        // Validate the Imposto in the database
        List<Imposto> impostoList = impostoRepository.findAll();
        assertThat(impostoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllImpostos() throws Exception {
        // Initialize the database
        impostoRepository.saveAndFlush(imposto);

        // Get all the impostoList
        restImpostoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(imposto.getId().intValue())))
            .andExpect(jsonPath("$.[*].tipoImposto").value(hasItem(DEFAULT_TIPO_IMPOSTO.toString())))
            .andExpect(jsonPath("$.[*].valorDevido").value(hasItem(DEFAULT_VALOR_DEVIDO.doubleValue())))
            .andExpect(jsonPath("$.[*].dataVencimento").value(hasItem(DEFAULT_DATA_VENCIMENTO.toString())));
    }

    @Test
    @Transactional
    void getImposto() throws Exception {
        // Initialize the database
        impostoRepository.saveAndFlush(imposto);

        // Get the imposto
        restImpostoMockMvc
            .perform(get(ENTITY_API_URL_ID, imposto.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(imposto.getId().intValue()))
            .andExpect(jsonPath("$.tipoImposto").value(DEFAULT_TIPO_IMPOSTO.toString()))
            .andExpect(jsonPath("$.valorDevido").value(DEFAULT_VALOR_DEVIDO.doubleValue()))
            .andExpect(jsonPath("$.dataVencimento").value(DEFAULT_DATA_VENCIMENTO.toString()));
    }

    @Test
    @Transactional
    void getNonExistingImposto() throws Exception {
        // Get the imposto
        restImpostoMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewImposto() throws Exception {
        // Initialize the database
        impostoRepository.saveAndFlush(imposto);

        int databaseSizeBeforeUpdate = impostoRepository.findAll().size();

        // Update the imposto
        Imposto updatedImposto = impostoRepository.findById(imposto.getId()).get();
        // Disconnect from session so that the updates on updatedImposto are not directly saved in db
        em.detach(updatedImposto);
        updatedImposto.tipoImposto(UPDATED_TIPO_IMPOSTO).valorDevido(UPDATED_VALOR_DEVIDO).dataVencimento(UPDATED_DATA_VENCIMENTO);

        restImpostoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedImposto.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedImposto))
            )
            .andExpect(status().isOk());

        // Validate the Imposto in the database
        List<Imposto> impostoList = impostoRepository.findAll();
        assertThat(impostoList).hasSize(databaseSizeBeforeUpdate);
        Imposto testImposto = impostoList.get(impostoList.size() - 1);
        assertThat(testImposto.getTipoImposto()).isEqualTo(UPDATED_TIPO_IMPOSTO);
        assertThat(testImposto.getValorDevido()).isEqualTo(UPDATED_VALOR_DEVIDO);
        assertThat(testImposto.getDataVencimento()).isEqualTo(UPDATED_DATA_VENCIMENTO);
    }

    @Test
    @Transactional
    void putNonExistingImposto() throws Exception {
        int databaseSizeBeforeUpdate = impostoRepository.findAll().size();
        imposto.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restImpostoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, imposto.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(imposto))
            )
            .andExpect(status().isBadRequest());

        // Validate the Imposto in the database
        List<Imposto> impostoList = impostoRepository.findAll();
        assertThat(impostoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchImposto() throws Exception {
        int databaseSizeBeforeUpdate = impostoRepository.findAll().size();
        imposto.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restImpostoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(imposto))
            )
            .andExpect(status().isBadRequest());

        // Validate the Imposto in the database
        List<Imposto> impostoList = impostoRepository.findAll();
        assertThat(impostoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamImposto() throws Exception {
        int databaseSizeBeforeUpdate = impostoRepository.findAll().size();
        imposto.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restImpostoMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(imposto)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Imposto in the database
        List<Imposto> impostoList = impostoRepository.findAll();
        assertThat(impostoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateImpostoWithPatch() throws Exception {
        // Initialize the database
        impostoRepository.saveAndFlush(imposto);

        int databaseSizeBeforeUpdate = impostoRepository.findAll().size();

        // Update the imposto using partial update
        Imposto partialUpdatedImposto = new Imposto();
        partialUpdatedImposto.setId(imposto.getId());

        partialUpdatedImposto.tipoImposto(UPDATED_TIPO_IMPOSTO);

        restImpostoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedImposto.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedImposto))
            )
            .andExpect(status().isOk());

        // Validate the Imposto in the database
        List<Imposto> impostoList = impostoRepository.findAll();
        assertThat(impostoList).hasSize(databaseSizeBeforeUpdate);
        Imposto testImposto = impostoList.get(impostoList.size() - 1);
        assertThat(testImposto.getTipoImposto()).isEqualTo(UPDATED_TIPO_IMPOSTO);
        assertThat(testImposto.getValorDevido()).isEqualTo(DEFAULT_VALOR_DEVIDO);
        assertThat(testImposto.getDataVencimento()).isEqualTo(DEFAULT_DATA_VENCIMENTO);
    }

    @Test
    @Transactional
    void fullUpdateImpostoWithPatch() throws Exception {
        // Initialize the database
        impostoRepository.saveAndFlush(imposto);

        int databaseSizeBeforeUpdate = impostoRepository.findAll().size();

        // Update the imposto using partial update
        Imposto partialUpdatedImposto = new Imposto();
        partialUpdatedImposto.setId(imposto.getId());

        partialUpdatedImposto.tipoImposto(UPDATED_TIPO_IMPOSTO).valorDevido(UPDATED_VALOR_DEVIDO).dataVencimento(UPDATED_DATA_VENCIMENTO);

        restImpostoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedImposto.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedImposto))
            )
            .andExpect(status().isOk());

        // Validate the Imposto in the database
        List<Imposto> impostoList = impostoRepository.findAll();
        assertThat(impostoList).hasSize(databaseSizeBeforeUpdate);
        Imposto testImposto = impostoList.get(impostoList.size() - 1);
        assertThat(testImposto.getTipoImposto()).isEqualTo(UPDATED_TIPO_IMPOSTO);
        assertThat(testImposto.getValorDevido()).isEqualTo(UPDATED_VALOR_DEVIDO);
        assertThat(testImposto.getDataVencimento()).isEqualTo(UPDATED_DATA_VENCIMENTO);
    }

    @Test
    @Transactional
    void patchNonExistingImposto() throws Exception {
        int databaseSizeBeforeUpdate = impostoRepository.findAll().size();
        imposto.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restImpostoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, imposto.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(imposto))
            )
            .andExpect(status().isBadRequest());

        // Validate the Imposto in the database
        List<Imposto> impostoList = impostoRepository.findAll();
        assertThat(impostoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchImposto() throws Exception {
        int databaseSizeBeforeUpdate = impostoRepository.findAll().size();
        imposto.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restImpostoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(imposto))
            )
            .andExpect(status().isBadRequest());

        // Validate the Imposto in the database
        List<Imposto> impostoList = impostoRepository.findAll();
        assertThat(impostoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamImposto() throws Exception {
        int databaseSizeBeforeUpdate = impostoRepository.findAll().size();
        imposto.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restImpostoMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(imposto)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Imposto in the database
        List<Imposto> impostoList = impostoRepository.findAll();
        assertThat(impostoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteImposto() throws Exception {
        // Initialize the database
        impostoRepository.saveAndFlush(imposto);

        int databaseSizeBeforeDelete = impostoRepository.findAll().size();

        // Delete the imposto
        restImpostoMockMvc
            .perform(delete(ENTITY_API_URL_ID, imposto.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Imposto> impostoList = impostoRepository.findAll();
        assertThat(impostoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
