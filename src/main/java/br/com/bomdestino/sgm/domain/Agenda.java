package br.com.bomdestino.sgm.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;

/**
 * A Agenda.
 */
@Entity
@Table(name = "agenda")
public class Agenda implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "data")
    private LocalDate data;

    @Column(name = "descricao_previa")
    private String descricaoPrevia;

    @JsonIgnoreProperties(value = { "agenda", "area" }, allowSetters = true)
    @OneToOne(mappedBy = "agenda")
    private Solicitacao solicitacao;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Agenda id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getData() {
        return this.data;
    }

    public Agenda data(LocalDate data) {
        this.setData(data);
        return this;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getDescricaoPrevia() {
        return this.descricaoPrevia;
    }

    public Agenda descricaoPrevia(String descricaoPrevia) {
        this.setDescricaoPrevia(descricaoPrevia);
        return this;
    }

    public void setDescricaoPrevia(String descricaoPrevia) {
        this.descricaoPrevia = descricaoPrevia;
    }

    public Solicitacao getSolicitacao() {
        return this.solicitacao;
    }

    public void setSolicitacao(Solicitacao solicitacao) {
        if (this.solicitacao != null) {
            this.solicitacao.setAgenda(null);
        }
        if (solicitacao != null) {
            solicitacao.setAgenda(this);
        }
        this.solicitacao = solicitacao;
    }

    public Agenda solicitacao(Solicitacao solicitacao) {
        this.setSolicitacao(solicitacao);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Agenda)) {
            return false;
        }
        return id != null && id.equals(((Agenda) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Agenda{" +
            "id=" + getId() +
            ", data='" + getData() + "'" +
            ", descricaoPrevia='" + getDescricaoPrevia() + "'" +
            "}";
    }
}
