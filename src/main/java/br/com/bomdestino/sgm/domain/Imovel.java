package br.com.bomdestino.sgm.domain;

import br.com.bomdestino.sgm.domain.enumeration.TipoImovel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A Imovel.
 */
@Entity
@Table(name = "imovel")
public class Imovel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "cod_imovel")
    private String codImovel;

    @Column(name = "documento_proprietario")
    private String documentoProprietario;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_imovel")
    private TipoImovel tipoImovel;

    @JsonIgnoreProperties(value = { "imovel" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private Endereco endereco;

    @OneToMany(mappedBy = "imovel")
    @JsonIgnoreProperties(value = { "imovel" }, allowSetters = true)
    private Set<Imposto> impostos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Imovel id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodImovel() {
        return this.codImovel;
    }

    public Imovel codImovel(String codImovel) {
        this.setCodImovel(codImovel);
        return this;
    }

    public void setCodImovel(String codImovel) {
        this.codImovel = codImovel;
    }

    public String getDocumentoProprietario() {
        return this.documentoProprietario;
    }

    public Imovel documentoProprietario(String documentoProprietario) {
        this.setDocumentoProprietario(documentoProprietario);
        return this;
    }

    public void setDocumentoProprietario(String documentoProprietario) {
        this.documentoProprietario = documentoProprietario;
    }

    public TipoImovel getTipoImovel() {
        return this.tipoImovel;
    }

    public Imovel tipoImovel(TipoImovel tipoImovel) {
        this.setTipoImovel(tipoImovel);
        return this;
    }

    public void setTipoImovel(TipoImovel tipoImovel) {
        this.tipoImovel = tipoImovel;
    }

    public Endereco getEndereco() {
        return this.endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public Imovel endereco(Endereco endereco) {
        this.setEndereco(endereco);
        return this;
    }

    public Set<Imposto> getImpostos() {
        return this.impostos;
    }

    public void setImpostos(Set<Imposto> impostos) {
        if (this.impostos != null) {
            this.impostos.forEach(i -> i.setImovel(null));
        }
        if (impostos != null) {
            impostos.forEach(i -> i.setImovel(this));
        }
        this.impostos = impostos;
    }

    public Imovel impostos(Set<Imposto> impostos) {
        this.setImpostos(impostos);
        return this;
    }

    public Imovel addImposto(Imposto imposto) {
        this.impostos.add(imposto);
        imposto.setImovel(this);
        return this;
    }

    public Imovel removeImposto(Imposto imposto) {
        this.impostos.remove(imposto);
        imposto.setImovel(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Imovel)) {
            return false;
        }
        return id != null && id.equals(((Imovel) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Imovel{" +
            "id=" + getId() +
            ", codImovel='" + getCodImovel() + "'" +
            ", documentoProprietario='" + getDocumentoProprietario() + "'" +
            ", tipoImovel='" + getTipoImovel() + "'" +
            "}";
    }
}
