package br.com.bomdestino.sgm.domain;

import br.com.bomdestino.sgm.domain.enumeration.TipoSolicitacao;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;

/**
 * A Solicitacao.
 */
@Entity
@Table(name = "solicitacao")
public class Solicitacao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "cod_solicitacao")
    private String codSolicitacao;

    @Column(name = "cod_usuario")
    private String codUsuario;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_solicitacao")
    private TipoSolicitacao tipoSolicitacao;

    @JsonIgnoreProperties(value = { "solicitacao" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private Agenda agenda;

    @JsonIgnoreProperties(value = { "solicitacao" }, allowSetters = true)
    @OneToOne(mappedBy = "solicitacao")
    private Area area;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Solicitacao id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodSolicitacao() {
        return this.codSolicitacao;
    }

    public Solicitacao codSolicitacao(String codSolicitacao) {
        this.setCodSolicitacao(codSolicitacao);
        return this;
    }

    public void setCodSolicitacao(String codSolicitacao) {
        this.codSolicitacao = codSolicitacao;
    }

    public String getCodUsuario() {
        return this.codUsuario;
    }

    public Solicitacao codUsuario(String codUsuario) {
        this.setCodUsuario(codUsuario);
        return this;
    }

    public void setCodUsuario(String codUsuario) {
        this.codUsuario = codUsuario;
    }

    public TipoSolicitacao getTipoSolicitacao() {
        return this.tipoSolicitacao;
    }

    public Solicitacao tipoSolicitacao(TipoSolicitacao tipoSolicitacao) {
        this.setTipoSolicitacao(tipoSolicitacao);
        return this;
    }

    public void setTipoSolicitacao(TipoSolicitacao tipoSolicitacao) {
        this.tipoSolicitacao = tipoSolicitacao;
    }

    public Agenda getAgenda() {
        return this.agenda;
    }

    public void setAgenda(Agenda agenda) {
        this.agenda = agenda;
    }

    public Solicitacao agenda(Agenda agenda) {
        this.setAgenda(agenda);
        return this;
    }

    public Area getArea() {
        return this.area;
    }

    public void setArea(Area area) {
        if (this.area != null) {
            this.area.setSolicitacao(null);
        }
        if (area != null) {
            area.setSolicitacao(this);
        }
        this.area = area;
    }

    public Solicitacao area(Area area) {
        this.setArea(area);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Solicitacao)) {
            return false;
        }
        return id != null && id.equals(((Solicitacao) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Solicitacao{" +
            "id=" + getId() +
            ", codSolicitacao='" + getCodSolicitacao() + "'" +
            ", codUsuario='" + getCodUsuario() + "'" +
            ", tipoSolicitacao='" + getTipoSolicitacao() + "'" +
            "}";
    }
}
