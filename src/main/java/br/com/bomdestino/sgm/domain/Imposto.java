package br.com.bomdestino.sgm.domain;

import br.com.bomdestino.sgm.domain.enumeration.TipoImposto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;

/**
 * A Imposto.
 */
@Entity
@Table(name = "imposto")
public class Imposto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_imposto")
    private TipoImposto tipoImposto;

    @Column(name = "valor_devido")
    private Double valorDevido;

    @Column(name = "data_vencimento")
    private LocalDate dataVencimento;

    @ManyToOne
    @JsonIgnoreProperties(value = { "endereco", "impostos" }, allowSetters = true)
    private Imovel imovel;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Imposto id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoImposto getTipoImposto() {
        return this.tipoImposto;
    }

    public Imposto tipoImposto(TipoImposto tipoImposto) {
        this.setTipoImposto(tipoImposto);
        return this;
    }

    public void setTipoImposto(TipoImposto tipoImposto) {
        this.tipoImposto = tipoImposto;
    }

    public Double getValorDevido() {
        return this.valorDevido;
    }

    public Imposto valorDevido(Double valorDevido) {
        this.setValorDevido(valorDevido);
        return this;
    }

    public void setValorDevido(Double valorDevido) {
        this.valorDevido = valorDevido;
    }

    public LocalDate getDataVencimento() {
        return this.dataVencimento;
    }

    public Imposto dataVencimento(LocalDate dataVencimento) {
        this.setDataVencimento(dataVencimento);
        return this;
    }

    public void setDataVencimento(LocalDate dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public Imovel getImovel() {
        return this.imovel;
    }

    public void setImovel(Imovel imovel) {
        this.imovel = imovel;
    }

    public Imposto imovel(Imovel imovel) {
        this.setImovel(imovel);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Imposto)) {
            return false;
        }
        return id != null && id.equals(((Imposto) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Imposto{" +
            "id=" + getId() +
            ", tipoImposto='" + getTipoImposto() + "'" +
            ", valorDevido=" + getValorDevido() +
            ", dataVencimento='" + getDataVencimento() + "'" +
            "}";
    }
}
