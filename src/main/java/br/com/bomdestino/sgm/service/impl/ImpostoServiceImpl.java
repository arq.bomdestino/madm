package br.com.bomdestino.sgm.service.impl;

import br.com.bomdestino.sgm.domain.Imposto;
import br.com.bomdestino.sgm.repository.ImpostoRepository;
import br.com.bomdestino.sgm.service.ImpostoService;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Imposto}.
 */
@Service
@Transactional
public class ImpostoServiceImpl implements ImpostoService {

    private final Logger log = LoggerFactory.getLogger(ImpostoServiceImpl.class);

    private final ImpostoRepository impostoRepository;

    public ImpostoServiceImpl(ImpostoRepository impostoRepository) {
        this.impostoRepository = impostoRepository;
    }

    @Override
    public Imposto save(Imposto imposto) {
        log.debug("Request to save Imposto : {}", imposto);
        return impostoRepository.save(imposto);
    }

    @Override
    public Optional<Imposto> partialUpdate(Imposto imposto) {
        log.debug("Request to partially update Imposto : {}", imposto);

        return impostoRepository
            .findById(imposto.getId())
            .map(existingImposto -> {
                if (imposto.getTipoImposto() != null) {
                    existingImposto.setTipoImposto(imposto.getTipoImposto());
                }
                if (imposto.getValorDevido() != null) {
                    existingImposto.setValorDevido(imposto.getValorDevido());
                }
                if (imposto.getDataVencimento() != null) {
                    existingImposto.setDataVencimento(imposto.getDataVencimento());
                }

                return existingImposto;
            })
            .map(impostoRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Imposto> findAll() {
        log.debug("Request to get all Impostos");
        return impostoRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Imposto> findOne(Long id) {
        log.debug("Request to get Imposto : {}", id);
        return impostoRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Imposto : {}", id);
        impostoRepository.deleteById(id);
    }
}
